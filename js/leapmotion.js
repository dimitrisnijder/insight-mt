    // Get the canvas DOM element 
    var canvas = document.getElementById('canvas');

    // Making sure we have the proper aspect ratio for our canvas
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;

    // Create the context we will use for drawing
    var c =  canvas.getContext('2d');

    // Save the canvas width and canvas height
    // as easily accesible variables
    var width = canvas.width;
    var height = canvas.height;

    // Creating a global Frame variable that we can access
    // throughout the program
    var frame;

    // Global keyTap and screenTap arrays
    var keyTaps = [];
    var KEYTAP_LIFETIME   =  .5;
    var KEYTAP_START_SIZE = 15;

    var screenTaps = [];
    var SCREENTAP_LIFETIME   = 1;
    var SCREENTAP_START_SIZE = 30;


    /*
      
      The leapToScene function takes a position in leap space 
      and converts it to the space in the canvas.
      
      It does this by using the interaction box, in order to 
      make sure that every part of the canvas is accesible 
      in the interaction area of the leap

    */

    function leapToScene( leapPos ){

      // Gets the interaction box of the current frame
      var iBox = frame.interactionBox;

      // Gets the left border and top border of the box
      // In order to convert the position to the proper
      // location for the canvas
      var left = iBox.center[0] - iBox.size[0]/2;
      var top = iBox.center[1] + iBox.size[1]/2;

      // Takes our leap coordinates, and changes them so
      // that the origin is in the top left corner 
      var x = leapPos[0] - left;
      var y = leapPos[1] - top;

      // Divides the position by the size of the box
      // so that x and y values will range from 0 to 1
      // as they lay within the interaction box
      x /= iBox.size[0];
      y /= iBox.size[1];

      // Uses the height and width of the canvas to scale
      // the x and y coordinates in a way that they 
      // take up the entire canvas
      x *= width;
      y *= height;

      // Returns the values, making sure to negate the sign 
      // of the y coordinate, because the y basis in canvas 
      // points down instead of up
      return [ x , -y ];

    }

    function convertFrame( frame , leapPos ){

      // Gets the interaction box of the current frame
      var iBox = frame.interactionBox;

      if(iBox){

        // Gets the left border and top border of the box
        // In order to convert the position to the proper
        // location for the canvas
        var left = iBox.center[0] - iBox.size[0]/2;
        var top = iBox.center[1] + iBox.size[1]/2;

        // Takes our leap coordinates, and changes them so
        // that the origin is in the top left corner 
        var x = leapPos[0] - left;
        var y = leapPos[1] - top;

        // Divides the position by the size of the box
        // so that x and y values will range from 0 to 1
        // as they lay within the interaction box
        x /= iBox.size[0];
        y /= iBox.size[1];

        // Uses the height and width of the canvas to scale
        // the x and y coordinates in a way that they 
        // take up the entire canvas
        x *= width;
        y *= height;

        // Returns the values, making sure to negate the sign 
        // of the y coordinate, because the y basis in canvas 
        // points down instead of up
        return [ x , -y ];
      }

    }


    function onSwipe( gesture ){

        var startPos = leapToScene( gesture.startPosition );
        var pos      = leapToScene( gesture.position );

//        console.log(gesture);

      switch (gesture.state) {
          case "start":
              //Handle starting gestures
              break;
          case "update":
              //Handle continuing gestures
              break;
          case "stop":
            var isHorizontal = Math.abs(gesture.direction[0]) > Math.abs(gesture.direction[1]);
              //Classify as right-left or up-down
              if(isHorizontal){
                  if(gesture.direction[0] > 0){
                      swipeDirection = "right";
                      $( ".pxs_prev" ).trigger( "click" );
                  } else {
                      swipeDirection = "left";
                      $( ".pxs_next" ).trigger( "click" );
                  }
              } else { //vertical
                  if(gesture.direction[1] > 0){
                      swipeDirection = "up";
                      window.location.href = "index.html";
                  } else {
                      swipeDirection = "down";
                  }                  
              }

              //console.log(swipeDirection);
            
              //Handle ending gestures
              break;
          default:
              //Handle unrecognized states
              break;
      }
      //console.log("Swiped!");
      //$( ".pxs_next" ).trigger( "click" );
      //window.location.hash = "swipe";

    }

    function onKeyTap( gesture ){

        $('#showWebsite').focus();
        var iframe = $('#showWebsite').contents().get(0);

        var pos  = leapToScene( gesture.position );
        var time = frame.timestamp;

        var el = document.elementFromPoint(pos[0], pos[1]);
        console.log(el);
        if (el instanceof HTMLIFrameElement) {
          el = el.contentWindow.document.elementFromPoint(pos[0], pos[1]); 
        }

        //console.log(el);

        // var e = new jQuery.Event("click");
        // e.pageX = pos[0];
        // e.pageY = pos[1];
        // $(iframe).trigger(e);

        //click(pos[0], pos[1]);

        // var link = $( ".pxs_thumbnails" ).find( "li.selected" ).find( "img" ).attr("data-link");
        
        // window.location.href = link;

        //console.log(link);

    }

  //   function click(x,y){
  //     var ev = document.createEvent("MouseEvent");
  //     var el = $('#showWebsite').contents().get(0);
  //     ev.initMouseEvent(
  //         "click",
  //         true /* bubble */, true  cancelable ,
  //         window, null,
  //         x, y, 0, 0, /* coordinates */
  //         false, false, false, false, /* modifier keys */
  //         0 /*left*/, null
  //     );
  //     el.dispatchEvent(ev);
  //     console.log(ev);
  // }


    // Creates our Leap Controller
    var controller = new Leap.Controller({enableGestures:true});

    // Tells the controller what to do every time it sees a frame
    controller.on( 'frame' , function( data ){

      // Assigning the data to the global frame object
      frame = data;
      
      // Clearing the drawing from the previous frame
      c.clearRect( 0 , 0 , width , height );

      for( var i = 0; i < frame.gestures.length; i++ ){

        var gesture = frame.gestures[i];


        var type = gesture.type;
        
        switch( type ){
            
          // case "swipe":
          //   onSwipe( gesture );
          //   break;
          
          case "keyTap":
            onKeyTap( gesture );
            break;
   
        }

      }

      // First we loop through all of the hands that the frame sees
      for( var i=0; i < 1; i++ ){

        // For each hand we define it
        var hand = frame.hands[i];

        if(hand){

          // and get its position, so that it can be passed through
          // for drawing the connections
          var handPos = convertFrame( frame , hand.palmPosition );

          // Loop through all the fingers of the hand we are on
          for( var j = 0; j < 1; j++ ){
            if(hand.fingers[j]){


              // Define the finger we are looking at
              var finger = hand.fingers[j];

              // and get its position in Canvas
              var fingerPos = convertFrame( frame , finger.tipPosition );


              /*
                  Second Draw the Finger
              */

              // Draw a full circle of radius 6 at the finger position
              //c.drawImage( img , fingerPos[0], fingerPos[1]);  

              //DRAW POSITION
              //$('#position').html("x position: " + fingerPos[0] + "\n y position: " + fingerPos[1]);
            }
          } 
        }
      }

    });
    
    // Get frames rolling by connecting the controller
    controller.connect();