jQuery.fn.css = (function (css2) {
    return function () {
        if (arguments.length) {
            return css2.apply(this, arguments);
        }
        var attr = ['font-family', 'font-size', 'font-weight', 'font-style', 'color',
            'text-transform', 'text-decoration',
            'line-height', 'text-align', 'vertical-align', 'background-color',
            'background-image', 'background-repeat', 'background-position',
            'opacity', 'width', 'height', 'top', 'right', 'bottom',
            'left', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left',
            'padding-top', 'padding-right', 'padding-bottom', 'padding-left',
            'border', 'position',
            'display', 'visibility', 'float'
        ];
        var len = attr.length,
            obj = {};
        for (var i = 0; i < len; i++) {
            obj[attr[i]] = css2.call(this, attr[i]);
        }
        return obj;
    };
})(jQuery.fn.css);


// function highlight(keywords, text) {
  
//     var words = keywords || [];
//     var regex = RegExp(words, 'gi');
//     var replacement = '<span class="highlighted">$&</span>';
  
//     return text.replace(regex, replacement);

// };

$.fn.wrapInTag = function(opts) {
  
  var words = opts.words || [],
      replacement = '<span class="highlighted">$&</span>';

      var string = words.toString().replace(/[-[\]{}()\/*+?.,\\^$|#\s]/g,"\\$&");
      var regex = RegExp(string, 'gi');

      console.log("string: " + string);
      console.log("regex: " + regex);
  
  return this.text(function() {
    return $(this).text().replace(regex, replacement);
  });
};

function htmlEncode(value) {
    return $('<div/>').text(value).html();
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

$(window).load(function () {

    $( ".terminal" ).scroll(function() {
        $('.jquery-line').remove();
    }); 

    var source = "";

    var websiteContent = $('#showWebsite').attr('src');

    $.get(websiteContent, function(data) {

        source = data;

        $('.code').text(data);
        prettyPrint();
    });

    var iframe = $('#showWebsite').contents().get(0);

    $(iframe).scroll(function() {
        $('.jquery-line').remove();
    }); 

    $('#showWebsite').contents().find("html").css('cursor', 'pointer');

    $(iframe).bind('click', function( event ) {
        event.preventDefault();

        //Removes drawed lines
        $('.jquery-line').remove();

        event.preventDefault();
        var element = event.target.outerHTML;
        var parent = event.target.parentElement;
        var node = event.target.nodeName;

        console.log(element);

        if (event.target.className != '') {
            var cl = "." + event.target.className;
        } else {
            var cl = event.target.className;
        }

        if (event.target.id != '') {
            var id = "#" + event.target.id;
        } else {
            var id = event.target.id;
        }

        $('.code').wrapInTag({
          words: [element]
        });


        var splitted = $('.code').text().split('<span class="highlighted">' + element + '</span>');

        //data tot en met het element
        $('.code').text(splitted[0]);
        //de highlighter
        $('.code').append('<span class="highlighted">' + htmlEncode(element) + '</span>');
        //de rest van de data
        $('.code').append(htmlEncode(splitted[1]));


        $('.code').removeClass('prettyprinted');

        PR.prettyPrint();

        $(".highlighted").scrollintoview({
            duration: 1000,
            direction: "vertical",
            complete: function () {

                var css = $('#showWebsite').contents().find(node + cl + id).css();
                var text = '';
                
                $('#line').html();
                var p = $( ".highlighted" );
                var offset = p.offset();

                var posY = offset.top - $(window).scrollTop();
                var posX = offset.left - $(window).scrollLeft(); 

                posX = posX + p.width();

                //linedraw(posX,posY, event.screenX, event.screenY);

                //$.line({x:posX, y:posY+20}, {x:event.screenX, y:event.screenY - 117}, {lineWidth: 2, lineColor: 'red'})

                for (property in css) {
                    text += property + ': ' + css[property] + ';\r\n';
                }

                $('.css-code').text(text);

                $('.css-code').removeClass('prettyprinted');

                PR.prettyPrint();
            }
        });

    });
});
