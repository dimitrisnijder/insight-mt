<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="//wilkesalex.github.io/leapstrap/dist/css/leapstrap.css" />
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/prettify.css" />

        <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700&Inconsolata' rel='stylesheet' type='text/css'>

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
        <?php
            require 'lib/simple_html_dom.php';
        ?>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <canvas id="canvas" style="z-index: -99999; position: absolute; top:0; width: 100%; height: 100%;"></canvas>
        <nav class="navigation-bar">
              <a class="leap-interactive left lm" leap-attractor="true" href="index.php">
                <i class="fa fa-chevron-left white"></i>
              </a> 

              <p>Insight MT - Project <?php echo $_GET['project'] ?></p>

              <!-- Modal -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true" leap-attractor="true">&times;</button>
                      <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div>
                    <div class="modal-body">
                      <h1>How-to!</h1>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->

            </nav>
        <div class="container">
            <div class="lefty">
                <div class="content">
                    <div class="window">  
                        <nav class="control-window">
                            <a class="closeit" href="#">close</a>
                            <a class="minimize" href="#">minimize</a>
                            <a class="deactivate" href="#">deactivate</a>
                        </nav>
                        <h1 class="titleInside">Terminal</h1>
                        <div class="terminal">
                            <!-- CODE HIER -->
                            <pre class="prettyprint code">
                            </pre>
                        </div>
                    </div>
                </div>
            </div>

            <div class="righty">
                <div class="content">
                    <div class="webframe">                      
                        <?php 
                            switch($_GET['project']){
                              case "1" :
                                $show = "first";
                                break;

                              case "2" :
                                $show = "second";
                                break;

                              case "3" :
                                $show = "third";
                                break;
                            }

                            echo '<iframe id="showWebsite" class="radius" border="0" src="sites/'.$show.'/index.html"></iframe>'
                            // $html = file_get_html('http://www.bvweijen.nl/');
                            // echo $html;
                        ?>

                        
                    </div>
                </div>
            </div>
                        <div class="middle">
                <div class="content">
                    <div class="css-window">  
                        <nav class="control-window">
                            <a class="closeit" href="#">close</a>
                            <a class="minimize" href="#">minimize</a>
                            <a class="deactivate" href="#">deactivate</a>
                        </nav>
                        <h1 class="titleInside">CSS</h1>
                        <div class="css-terminal">
                            <!-- CODE HIER -->
                            <pre class="prettyprint css-code">
                            </pre>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.js"><\/script>')</script>

        <script src="http://yandex.st/highlightjs/8.0/highlight.min.js"></script>
        <script type="text/javascript" src="js/google-code-prettify/prettify.js"></script>
        <script type="text/javascript" src="js/jquery.scrollintoview.js"></script>
        <script src="js/plugins.js"></script>
        <script src="lib/line/jquery.domline.js"></script>
        <script src="lib/line/lib/jquery.transform2d.min.js"></script>
        <script src="//js.leapmotion.com/0.3.0-beta3/leap.js"></script>
        <script src="//wilkesalex.github.io/leapstrap/dist/js/leapstrap.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>

        <script src="//js.leapmotion.com/0.2.0/leap.min.js"></script>

   <script>
//     // Get the canvas DOM element 
//     var canvas = document.getElementById('canvas');

//     // Making sure we have the proper aspect ratio for our canvas
//     canvas.width = canvas.clientWidth;
//     canvas.height = canvas.clientHeight;

//     // Create the context we will use for drawing
//     var c =  canvas.getContext('2d');
// var img = document.createElement('img');
// img.src = "sites/first/images/basic-logo.png";



//     // Save the canvas width and canvas height
//     // as easily accesible variables
//     var width = canvas.width;
//     var height = canvas.height;

//     // Creating a global Frame variable that we can access
//     // throughout the program
//     var frame;

//     // Global keyTap and screenTap arrays
//     var keyTaps = [];
//     var KEYTAP_LIFETIME   =  .5;
//     var KEYTAP_START_SIZE = 15;

//     var screenTaps = [];
//     var SCREENTAP_LIFETIME   = 1;
//     var SCREENTAP_START_SIZE = 30;


//     /*
      
//       The leapToScene function takes a position in leap space 
//       and converts it to the space in the canvas.
      
//       It does this by using the interaction box, in order to 
//       make sure that every part of the canvas is accesible 
//       in the interaction area of the leap

//     */

//     function leapToScene( leapPos ){

//       // Gets the interaction box of the current frame
//       var iBox = frame.interactionBox;

//       // Gets the left border and top border of the box
//       // In order to convert the position to the proper
//       // location for the canvas
//       var left = iBox.center[0] - iBox.size[0]/2;
//       var top = iBox.center[1] + iBox.size[1]/2;

//       // Takes our leap coordinates, and changes them so
//       // that the origin is in the top left corner 
//       var x = leapPos[0] - left;
//       var y = leapPos[1] - top;

//       // Divides the position by the size of the box
//       // so that x and y values will range from 0 to 1
//       // as they lay within the interaction box
//       x /= iBox.size[0];
//       y /= iBox.size[1];

//       // Uses the height and width of the canvas to scale
//       // the x and y coordinates in a way that they 
//       // take up the entire canvas
//       x *= width;
//       y *= height;

//       // Returns the values, making sure to negate the sign 
//       // of the y coordinate, because the y basis in canvas 
//       // points down instead of up
//       return [ x , -y ];

//     }

//     function onScreenTap( gesture ){

//       var pos  = leapToScene( gesture.position );
    
//       var time = frame.timestamp;

//         var iframe = $('#showWebsite').contents().get(0);

//         var style = $('.leap-pointable-cursor').attr('style');
//         //var style = $(data).filter('div').attr('style');
//         var styleObj = {};
//         $.each(style.split(';'),function(){
//             var rule = this.split(':');
//             styleObj[$.trim(rule[0])] = $.trim(rule[1]);
//         });

//         //console.log(styleObj.top);

//         // var element = iframe.elementFromPoint(styleObj.left, styleObj.top);

//         // var e = new jQuery.Event("click");
//         // e.pageX = styleObj.left;
//         // e.pageY = styleObj.top;
//         // $(iframe).trigger(e);

//         var elementUnder = iframe.elementFromPoint(styleObj.left,styleObj.top);

//         console.log(elementUnder);


//         //NEW 1
//         var iframeDoc = $('#showWebsite').contents().get(0);
//         var lastHovered = [false];

//         $(iframeDoc).mousemove(function(e) {
//             console.log("fr");
//         }).click(function(e) { 
//             var elementClicked = iframeDoc.elementFromPoint(e.clientX, e.clientY);        
//             alert("clicked element "+elementHovered);
//         });

//     }

//     // Creates our Leap Controller
//     var controller = new Leap.Controller({enableGestures:true});

//     // Tells the controller what to do every time it sees a frame
//     controller.on( 'frame' , function( data ){

//       // Assigning the data to the global frame object
//       frame = data;
      
//       for( var i = 0; i < frame.gestures.length; i++ ){

//         var gesture = frame.gestures[i];


//         var type = gesture.type;
        
//         switch( type ){
          
//           case "screenTap":
//             //onScreenTap( gesture );
//             break;
//         }

//       }


//     });
    
//     // Get frames rolling by connecting the controller
//     controller.connect();

        
//   </script>
    </body>
</html>
