<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Include Leapstrap CSS -->
        <link rel="stylesheet" href="//wilkesalex.github.io/leapstrap/dist/css/leapstrap.css" />
        <!-- Include jQuery (maybe you already do?) -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
        <!-- Include Leap (maybe you already do?) -->
        <script src="//js.leapmotion.com/0.3.0-beta3/leap.js"></script>
        <!-- Include Leapstrap JS -->
        <script src="//wilkesalex.github.io/leapstrap/dist/js/leapstrap.js"></script>

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="//wilkesalex.github.io/leapstrap/dist/css/leapstrap.css" />
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/prettify.css" />

        <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700&Inconsolata' rel='stylesheet' type='text/css'>

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
        <?php
            require 'lib/simple_html_dom.php';
        ?>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <canvas id="canvas" style="z-index:-99999; position: absolute; top:0; width: 100%; height: 100%;"></canvas>
        <nav class="navigation-bar">Insight MT</nav>
        <div id="slider">
          <div id="pxs_container" class="pxs_container">
            <div class="pxs_loading">Loading images...</div>
            <div class="pxs_slider_wrapper">
              <ul class="pxs_slider">
                <li><img src="images/screen3.jpg" alt="First Image" /></li>
                <li><img src="images/screen2.jpg" alt="Second Image" /></li>
                <li><img src="images/screen1.jpg" alt="Third Image" /></li>
              </ul>
              <div class="pxs_navigation">
                <span class="pxs_next leap-interactive" leap-attractor="true"></span>
                <span class="pxs_prev leap-interactive" leap-attractor="true"></span>
              </div>
              <ul class="pxs_thumbnails">
                <li class="leap-interactive" leap-attractor="true"><img src="images/screen3.jpg" alt="First Image" width="130" data-link="project.php?project=1"/></li>
                <li class="leap-interactive" leap-attractor="true"><img src="images/screen2.jpg" alt="Second Image" width="130" data-link="project.php?project=2"/></li>
                <li class="leap-interactive" leap-attractor="true"><img src="images/screen1.jpg" alt="Third Image" width="130" data-link="project.php?project=3"/></li>
              </ul>
            </div>
          </div>
        </div>

        <center>
          <a id="seeCode" href="#" class="btn btn-default">See code!</a>
        </center>
        

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.js"><\/script>')</script>
        <script src="http://yandex.st/highlightjs/8.0/highlight.min.js"></script>
        <script type="text/javascript" src="js/google-code-prettify/prettify.js"></script>
        <script type="text/javascript" src="js/jquery.scrollintoview.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/custom.js"></script>
        <script src="lib/line/jquery.domline.js"></script>
        <script src="lib/line/lib/jquery.transform2d.min.js"></script>
        <script src="//js.leapmotion.com/0.3.0-beta3/leap.js"></script>
        <script src="//wilkesalex.github.io/leapstrap/dist/js/leapstrap.js"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script src="//js.leapmotion.com/0.2.0/leap.min.js"></script>

  <script>
    // Get the canvas DOM element 
    var canvas = document.getElementById('canvas');

    // Making sure we have the proper aspect ratio for our canvas
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;

    // Create the context we will use for drawing
    var c =  canvas.getContext('2d');

    // Save the canvas width and canvas height
    // as easily accesible variables
    var width = canvas.width;
    var height = canvas.height;

    // Creating a global Frame variable that we can access
    // throughout the program
    var frame;

    // Global keyTap and screenTap arrays
    var keyTaps = [];
    var KEYTAP_LIFETIME   =  .5;
    var KEYTAP_START_SIZE = 15;

    var screenTaps = [];
    var SCREENTAP_LIFETIME   = 1;
    var SCREENTAP_START_SIZE = 30;


    /*
      
      The leapToScene function takes a position in leap space 
      and converts it to the space in the canvas.
      
      It does this by using the interaction box, in order to 
      make sure that every part of the canvas is accesible 
      in the interaction area of the leap

    */

    function leapToScene( leapPos ){

      // Gets the interaction box of the current frame
      var iBox = frame.interactionBox;

      // Gets the left border and top border of the box
      // In order to convert the position to the proper
      // location for the canvas
      var left = iBox.center[0] - iBox.size[0]/2;
      var top = iBox.center[1] + iBox.size[1]/2;

      // Takes our leap coordinates, and changes them so
      // that the origin is in the top left corner 
      var x = leapPos[0] - left;
      var y = leapPos[1] - top;

      // Divides the position by the size of the box
      // so that x and y values will range from 0 to 1
      // as they lay within the interaction box
      x /= iBox.size[0];
      y /= iBox.size[1];

      // Uses the height and width of the canvas to scale
      // the x and y coordinates in a way that they 
      // take up the entire canvas
      x *= width;
      y *= height;

      // Returns the values, making sure to negate the sign 
      // of the y coordinate, because the y basis in canvas 
      // points down instead of up
      return [ x , -y ];

    }

    function onSwipe( gesture ){

        var startPos = leapToScene( gesture.startPosition );
        var pos      = leapToScene( gesture.position );

//        console.log(gesture);

      switch (gesture.state) {
          case "start":
              //Handle starting gestures
              break;
          case "update":
              //Handle continuing gestures
              break;
          case "stop":
            var isHorizontal = Math.abs(gesture.direction[0]) > Math.abs(gesture.direction[1]);
              //Classify as right-left or up-down
              if(isHorizontal){
                  if(gesture.direction[0] > 0){
                      swipeDirection = "right";
                      $( ".pxs_prev" ).trigger( "click" );
                  } else {
                      swipeDirection = "left";
                      $( ".pxs_next" ).trigger( "click" );
                  }
                }
              // } else { //vertical
              //     if(gesture.direction[1] > 0){
              //         swipeDirection = "up";
              //        // window.location.href = "index.html";
              //     } else {
              //         swipeDirection = "down";
              //     }                  
              // }

              //console.log(swipeDirection);
            
              //Handle ending gestures
              break;
          default:
              //Handle unrecognized states
              break;
      }
      //console.log("Swiped!");
      //$( ".pxs_next" ).trigger( "click" );
      //window.location.hash = "swipe";

    }

    function onScreenTap( gesture ){

      var link = $( ".pxs_thumbnails" ).find( "li.selected" ).find( "img" ).attr("data-link");
      window.location.href = link;

    }

    // Creates our Leap Controller
    var controller = new Leap.Controller({enableGestures:true});

    // Tells the controller what to do every time it sees a frame
    controller.on( 'frame' , function( data ){

      // Assigning the data to the global frame object
      frame = data;
      
      for( var i = 0; i < frame.gestures.length; i++ ){

        var gesture = frame.gestures[i];


        var type = gesture.type;
        
        switch( type ){
          
          case "screenTap":
            onScreenTap( gesture );
            break;

          case "swipe":
            onSwipe(gesture);
            break;
        }

      }


    });
    
    // Get frames rolling by connecting the controller
    controller.connect();

        
    </script>
    <script type="text/javascript">
      $(function() {
        var $pxs_container  = $('#pxs_container');
        $pxs_container.parallaxSlider();

        $( "#seeCode" ).click(function() {
          var link = $( ".pxs_thumbnails" ).find( "li.selected" ).find( "img" ).attr("data-link");
            window.location.href = link;
        });  
      });
    </script>
    
    </body>
</html>
